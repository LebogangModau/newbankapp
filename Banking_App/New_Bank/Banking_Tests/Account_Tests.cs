
/*
 * [2019] - [2021] Eblocks Software (Pty) Ltd, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Eblocks
 * Software (Pty) Ltd.
 * and its suppliers (if any). The intellectual and technical concepts contained herein
 * are proprietary
 * to Eblocks Software (Pty) Ltd. and its suppliers (if any) and may be covered by South 
 * African, U.S.
 * and Foreign patents, patents in process, and are protected by trade secret and / or 
 * copyright law.
 * Dissemination of this information or reproduction of this material is forbidden unless
 * prior written
 * permission is obtained from Eblocks Software (Pty) Ltd.
*/

using System;
using Xunit;
using Banking_Transactions;

namespace Banking_Tests
{
  /// <summary>
  ///     provides tests for banking transactions
  /// </summary>
    public class Account_Tests
    {  
      /// <summary>
      ///     Provides test case to see if balance updates when we add money
      /// </summary>
       [Fact]
        public void Depositing_Funds_Updates_Balance()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT
            account.Deposit_Funds(100);

            // ASSERT
            Assert.Equal(1100, account.Balance);
        }
        /// <summary>
        ///     Provides test case to throw added negtaive funds
        /// </summary>
        [Fact]
        public void Depositing_Negative_Funds_Throws()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT + ASSERT
            Assert.Throws<ArgumentOutOfRangeException>(() => account.Deposit_Funds(-100));
        }
        /// <summary>
        ///      Provides test case to subtract funds from Balance
        /// </summary>
        [Fact]
        public void Withdrawing_Funds_Updates_Balance()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT
            account.Withdraw_Funds(100);

            // ASSERT
            Assert.Equal(900, account.Balance);
        }
        /// <summary>
        ///     Provides test case to throw added negtaive funds
        /// </summary>
        [Fact]
        public void Withdrawing_Negative_Funds_Throws()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT + ASSERT
            Assert.Throws<ArgumentOutOfRangeException>(() => account.Withdraw_Funds(-100));
        }
        /// <summary>
        ///     Provides test case to throw insufficient funds
        /// </summary>
        [Fact]
        public void Withdrawing_More_Than_Funds_Throws()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT + ASSERT
            Assert.Throws<ArgumentOutOfRangeException>(() => account.Withdraw_Funds(2000));
        }
        /// <summary>
        ///     Provides test case to tranfer funds to savings account
        /// </summary>
        [Fact]
        public void Transfering_Funds_Updates_Both_Accounts()
        {
            // ARRANGE
            var account = new Bank_Account(1000);
            var savingsAccount = new Bank_Account();

            // ACT
            account.TransferFundsTo(savingsAccount, 500);

            // ASSERT
            Assert.Equal(500, account.Balance);
            Assert.Equal(500, savingsAccount.Balance);
        }

        [Fact]
        public void TransferFundsTo_Non_Existing_Account_Throws()
        {
            // ARRANGE
            var account = new Bank_Account(1000);

            // ACT + ASSERT
            Assert.Throws<ArgumentNullException>(() => account.TransferFundsTo(null, 2000));
        }
    }
}
