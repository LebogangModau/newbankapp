﻿
/*
 * [2019] - [2021] Eblocks Software (Pty) Ltd, All Rights Reserved.
 * NOTICE: All information contained herein is, and remains the property of Eblocks
 * Software (Pty) Ltd.
 * and its suppliers (if any). The intellectual and technical concepts contained herein
 * are proprietary
 * to Eblocks Software (Pty) Ltd. and its suppliers (if any) and may be covered by South 
 * African, U.S.
 * and Foreign patents, patents in process, and are protected by trade secret and / or 
 * copyright law.
 * Dissemination of this information or reproduction of this material is forbidden unless
 * prior written
 * permission is obtained from Eblocks Software (Pty) Ltd.
*/

using System;

namespace Banking_Transactions
{
  /// <summary>
  ///     Provides parameters and methods for banking transactions.
  /// </summary>
  public class Bank_Account
  {
        private double balance;
        public Bank_Account()
        {
        }
        public Bank_Account(double balance)
        {
            this.balance = balance;
        }
        public double Balance
        {
            get { return balance; }
        }
        /// <summary>
        /// Method used to deposit funds from account.
        /// </summary>
        public void Deposit_Funds(double amount)
        {
            
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("Cannot deposit invalid amount");
            }
            return balance += amount;
        }
        /// <summary>
        ///     Method used to withdraw funds from account.
        /// </summary>
        public void Withdraw_Funds(double amount)
        { 
            if (amount > balance)
            {
                throw new ArgumentOutOfRangeException("insufficient funds, try a smaller amountin");
            }
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("insufficient funds");
            }
            return balance -= amount;
        }
        /// <summary>
        ///    Method used to transfer funds to savings account..
        /// </summary>
        /// <param name="savingsAccount"> Account we tranfers money to.</param>
        public void TransferFundsTo(Bank_Account savingsAccount, double amount)
        {
            if (savingsAccount is null)
            {
                throw new ArgumentNullException("no other account");
            }
            Withdraw_Funds(amount);
            savingsAccount.Deposit_Funds(amount);
        }
    static void Main(string[] args)
        {
            Bank_Account bank = new Bank_Account();
            Console.WriteLine("Your Current Account Balance {0}", bank.balance);
            Console.WriteLine("Press 1 to Make a Deposit, Press 2 to make a Withdraw, Press 3 to make a Transfer");
        }
  }
}
